proc pnsynth {} {
  cd C:/xilinks_projs/final_proj2/proc
  if { [xload xmp proc.xmp] != 0 } {
    return -1
  }
  if { [catch {run netlist} result] } {
    return -1
  }
  return 0
}
if { [catch {pnsynth} result] } {
  exit -1
}
exit $result
