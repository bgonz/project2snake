//////////////////////////////////////////////////////////////////////////////
// Filename:          C:\xilinks_projs\final_proj2\proc\drivers/snake_v2_01_a/examples/snake_v2_1_0_app.c
// Version:           2.01.a
// Description:       snake (new FSL core) Driver Example File
// Date:              Mon Dec 15 13:30:03 2008 (by Create and Import Peripheral Wizard)
//////////////////////////////////////////////////////////////////////////////

#include "snake.h"

#include "xparameters.h"

/*
* Follwing is an example driver function 
* that is called in the main function.
*
* This example driver writes all the data in the input arguments
* into the input FSL bus through blocking writes. FSL peripheral will
* automatically read from the FSL bus.
*
* CAUTION:
*
* The sequence of writes and reads in this function should be consistent
* with the sequence of reads or writes in the HDL implementation of this
* coprocessor.
*
*/
// Instance name specific MACROs. Defined for each instance of the peripheral.
#define WRITE_SNAKE_0(val)  write_into_fsl(val, XPAR_FSL_SNAKE_0_INPUT_SLOT_ID)

void snake_app(
       unsigned int* input_0      /* Array size = 1 */
       )
{
   int i;

   //Start writing into the FSL bus
   for (i=0; i<1; i++)
   {
      WRITE_SNAKE_0(input_0[i]);
   }
}

main()
{
	 unsigned int input_0[1];     


#ifdef __PPC__
	 // Enable APU for PowerPC.
	 unsigned int msr_i;
	 msr_i = mfmsr();
	 msr_i = (msr_i | XREG_MSR_APU_AVAILABLE | XREG_MSR_APU_ENABLE) & ~XREG_MSR_USER_MODE;
	 mtmsr(msr_i);
#endif

	 //Initialize your input data over here: 
	 input_0[0] = 12345;     

	 //Call the macro with instance specific slot IDs
	 snake(
		 XPAR_FSL_SNAKE_0_INPUT_SLOT_ID,
		 input_0    
		 );

	 // You can also define your own function to access the peripheral
	 // Here you are calling the example function defined above
	 // Note the slot ID can not be passed over as function parameters
	 snake_app(
		 input_0    
		 );

}

