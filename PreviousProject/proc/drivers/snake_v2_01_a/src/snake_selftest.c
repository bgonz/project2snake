//////////////////////////////////////////////////////////////////////////////
// Filename:          C:\xilinks_projs\final_proj2\proc\drivers/snake_v2_01_a/src/snake_selftest.c
// Version:           2.01.a
// Description:       
// Date:              Mon Dec 15 13:30:03 2008 (by Create and Import Peripheral Wizard)
//////////////////////////////////////////////////////////////////////////////

#include "xparameters.h"
#include "snake.h"

/* IMPORTANT:
*  In order to run this self test, you need to modify the value of following
*  micros according to the slot ID defined in xparameters.h file. 
*/
#define input_slot_id   XPAR_FSL_SNAKE_0_INPUT_SLOT_ID
XStatus SNAKE_SelfTest()
{
	 unsigned int input_0[1];     

	 //Initialize your input data over here: 
	 input_0[0] = 12345;     

	 //Call the macro with instance specific slot IDs
	 snake(
		 input_slot_id,
		 input_0    
		 );


	 return XST_SUCCESS;
}
