-------------------------------------------------------------------------------
-- proc_stub.vhd
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

entity proc_stub is
  port (
    fpga_0_Buttons_4Bit_GPIO_in_pin : in std_logic_vector(0 to 3);
    sys_clk_pin : in std_logic;
    sys_rst_pin : in std_logic;
    snake_0_rgb_pin : out std_logic_vector(0 to 2);
    snake_0_vsync_pin : out std_logic;
    snake_0_hsync_pin : out std_logic
  );
end proc_stub;

architecture STRUCTURE of proc_stub is

  component proc is
    port (
      fpga_0_Buttons_4Bit_GPIO_in_pin : in std_logic_vector(0 to 3);
      sys_clk_pin : in std_logic;
      sys_rst_pin : in std_logic;
      snake_0_rgb_pin : out std_logic_vector(0 to 2);
      snake_0_vsync_pin : out std_logic;
      snake_0_hsync_pin : out std_logic
    );
  end component;

begin

  proc_i : proc
    port map (
      fpga_0_Buttons_4Bit_GPIO_in_pin => fpga_0_Buttons_4Bit_GPIO_in_pin,
      sys_clk_pin => sys_clk_pin,
      sys_rst_pin => sys_rst_pin,
      snake_0_rgb_pin => snake_0_rgb_pin,
      snake_0_vsync_pin => snake_0_vsync_pin,
      snake_0_hsync_pin => snake_0_hsync_pin
    );

end architecture STRUCTURE;

