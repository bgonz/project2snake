-------------------------------------------------------------------------------
-- snake_0_wrapper.vhd
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

library snake_v2_01_a;
use snake_v2_01_a.all;

entity snake_0_wrapper is
  port (
    FSL_Clk : in std_logic;
    FSL_Rst : in std_logic;
    FSL_S_Clk : out std_logic;
    FSL_S_Read : out std_logic;
    FSL_S_Data : in std_logic_vector(0 to 31);
    FSL_S_Control : in std_logic;
    FSL_S_Exists : in std_logic;
    hsync : out std_logic;
    vsync : out std_logic;
    rgb : out std_logic_vector(0 to 2)
  );

  attribute x_core_info : STRING;
  attribute x_core_info of snake_0_wrapper : entity is "snake_v2_01_a";

end snake_0_wrapper;

architecture STRUCTURE of snake_0_wrapper is

  component snake is
    port (
      FSL_Clk : in std_logic;
      FSL_Rst : in std_logic;
      FSL_S_Clk : out std_logic;
      FSL_S_Read : out std_logic;
      FSL_S_Data : in std_logic_vector(0 to 31);
      FSL_S_Control : in std_logic;
      FSL_S_Exists : in std_logic;
      hsync : out std_logic;
      vsync : out std_logic;
      rgb : out std_logic_vector(0 to 2)
    );
  end component;

begin

  snake_0 : snake
    port map (
      FSL_Clk => FSL_Clk,
      FSL_Rst => FSL_Rst,
      FSL_S_Clk => FSL_S_Clk,
      FSL_S_Read => FSL_S_Read,
      FSL_S_Data => FSL_S_Data,
      FSL_S_Control => FSL_S_Control,
      FSL_S_Exists => FSL_S_Exists,
      hsync => hsync,
      vsync => vsync,
      rgb => rgb
    );

end architecture STRUCTURE;

