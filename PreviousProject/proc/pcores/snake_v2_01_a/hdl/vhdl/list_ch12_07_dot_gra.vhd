--Listing 12.7
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
entity bitmap_gen is
   port(
        clk, reset: in std_logic;
        video_on: in std_logic;
		  p_tick: in std_logic;
        unit_x,unit_y: in std_logic_vector(4 downto 0);
		  dot_x, dot_y: in std_logic_vector(4 downto 0);
		  dot_val: in std_logic_vector(0 downto 0);
        bit_rgb: out std_logic_vector(2 downto 0)
   );
end bitmap_gen;

architecture dual_port_ram_arch of bitmap_gen is
   signal pix_x, pix_y: unsigned(4 downto 0);
   signal refr_tick: std_logic;
   signal load_tick: std_logic;
   ----------------------------------------------
   -- video sram
   ----------------------------------------------
   signal we: std_logic;
   signal addr_r, addr_w: std_logic_vector(9 downto 0);
   signal din, dout: std_logic_vector(0 downto 0);
   ----------------------------------------------
   -- object output signals
   ----------------------------------------------
   signal bitmap_on: std_logic;
   signal bitmap_rgb: std_logic_vector(0 downto 0);
begin
   -- instantiate dual port video RAM (2^12-by-7)
   video_ram: entity work.xilinx_dual_port_ram_sync
      generic map(ADDR_WIDTH=>10, DATA_WIDTH=>1)
      port map(clk=>clk, we=>we,
               addr_a=>addr_w, addr_b=>addr_r,
               din_a=>din, dout_a=>open, dout_b=>dout);
   -- video ram interface
   addr_w <= std_logic_vector(dot_y & dot_x);
   addr_r <=
      std_logic_vector(pix_y & pix_x);
   we <= '1';
   din <= dot_val;
   bitmap_rgb <= dout;
   -- misc. signals
   pix_x <= unsigned(unit_x);
   pix_y <= unsigned(unit_y);

   -- pixel within bit map area
  --   bitmap_on <=
   --   '1' when (pix_x<=31) and (pix_y<=23) else
    --  '0';
		
   -- dot position

   -- rgb multiplexing circuit
   process(video_on,bitmap_on,bitmap_rgb,p_tick)
   begin
      if video_on='0' then
          bit_rgb <= "000"; --blank
      else

            bit_rgb <= (bitmap_rgb & bitmap_rgb & bitmap_rgb);

      end if;
   end process;
end dual_port_ram_arch;

