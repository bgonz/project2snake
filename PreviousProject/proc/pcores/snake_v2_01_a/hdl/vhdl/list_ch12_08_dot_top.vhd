-- Listing 12.8
library ieee;
use ieee.std_logic_1164.all;
entity graphics_top is
   port (
      clk,reset: in std_logic;
      hsync, vsync: out  std_logic;
		dot_x, dot_y: in std_logic_vector(4 downto 0);
		dot_val: in std_logic_vector(0 downto 0);
      rgb: out std_logic_vector(2 downto 0)
   );
end graphics_top;

architecture arch of graphics_top is
   signal pixel_x, pixel_y: std_logic_vector(9 downto 0);
   signal video_on, pixel_tick: std_logic;
   signal rgb_reg, rgb_next: std_logic_vector(2 downto 0);
	signal unit_x, unit_y: std_logic_vector(4 downto 0);
begin
   -- instantiate VGA sync circuit
   vga_sync_unit: entity work.vga_sync
      port map(clk=>clk, reset=>reset,
               hsync=>hsync, vsync=>vsync,
               video_on=>video_on, p_tick=>pixel_tick,
               pixel_x=>pixel_x, pixel_y=>pixel_y);
	-- instantiate unit converter
   converter_unit: entity work.unit_converter
      port map(pixel_x=>pixel_x,
               pixel_y=>pixel_y,
					unit_x=>unit_x,
					unit_y=>unit_y);
   -- instantiate bit-map pixel generator
   bitmap_unit: entity work.bitmap_gen
      port map(clk=>clk, reset=>reset,
					p_tick => pixel_tick,
               video_on=>video_on, unit_x=>unit_x,
               unit_y=>unit_y, dot_x=>dot_x,
					dot_y=>dot_y, dot_val=>dot_val, bit_rgb=>rgb_next);
					
   -- rgb buffer
   process (clk)
   begin
      if (clk'event and clk='1') then
         if (pixel_tick='1') then
            rgb_reg <= rgb_next;
         end if;
      end if;
   end process;
   rgb <= rgb_reg;
end arch;