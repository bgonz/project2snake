------------------------------------------------------------------------------
-- snake - entity/architecture pair
------------------------------------------------------------------------------
--
-- ***************************************************************************
-- ** Copyright (c) 1995-2008 Xilinx, Inc.  All rights reserved.            **
-- **                                                                       **
-- ** Xilinx, Inc.                                                          **
-- ** XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS"         **
-- ** AS A COURTESY TO YOU, SOLELY FOR USE IN DEVELOPING PROGRAMS AND       **
-- ** SOLUTIONS FOR XILINX DEVICES.  BY PROVIDING THIS DESIGN, CODE,        **
-- ** OR INFORMATION AS ONE POSSIBLE IMPLEMENTATION OF THIS FEATURE,        **
-- ** APPLICATION OR STANDARD, XILINX IS MAKING NO REPRESENTATION           **
-- ** THAT THIS IMPLEMENTATION IS FREE FROM ANY CLAIMS OF INFRINGEMENT,     **
-- ** AND YOU ARE RESPONSIBLE FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE      **
-- ** FOR YOUR IMPLEMENTATION.  XILINX EXPRESSLY DISCLAIMS ANY              **
-- ** WARRANTY WHATSOEVER WITH RESPECT TO THE ADEQUACY OF THE               **
-- ** IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OR        **
-- ** REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE FROM CLAIMS OF       **
-- ** INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS       **
-- ** FOR A PARTICULAR PURPOSE.                                             **
-- **                                                                       **
-- ***************************************************************************
--
------------------------------------------------------------------------------
-- Filename:          snake
-- Version:           1.00.a
-- Description:       Example FSL core (VHDL).
-- Date:              Fri Dec 05 15:16:30 2008 (by Create and Import Peripheral Wizard)
-- VHDL Standard:     VHDL'93
------------------------------------------------------------------------------
-- Naming Conventions:
--   active low signals:                    "*_n"
--   clock signals:                         "clk", "clk_div#", "clk_#x"
--   reset signals:                         "rst", "rst_n"
--   generics:                              "C_*"
--   user defined types:                    "*_TYPE"
--   state machine next state:              "*_ns"
--   state machine current state:           "*_cs"
--   combinatorial signals:                 "*_com"
--   pipelined or register delay signals:   "*_d#"
--   counter signals:                       "*cnt*"
--   clock enable signals:                  "*_ce"
--   internal version of output port:       "*_i"
--   device pins:                           "*_pin"
--   ports:                                 "- Names begin with Uppercase"
--   processes:                             "*_PROCESS"
--   component instantiations:              "<ENTITY_>I_<#|FUNC>"
------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-------------------------------------------------------------------------------------
--
--
-- Definition of Ports
-- FSL_Clk             : Synchronous clock
-- FSL_Rst           : System reset, should always come from FSL bus
-- FSL_S_Clk       : Slave asynchronous clock
-- FSL_S_Read      : Read signal, requiring next available input to be read
-- FSL_S_Data      : Input data
-- FSL_S_CONTROL   : Control Bit, indicating the input data are control word
-- FSL_S_Exists    : Data Exist Bit, indicating data exist in the input FSL bus
--
-------------------------------------------------------------------------------

------------------------------------------------------------------------------
-- Entity Section
------------------------------------------------------------------------------

entity snake is
	port 
	(
		-- DO NOT EDIT BELOW THIS LINE ---------------------
		-- Bus protocol ports, do not add or delete. 
		FSL_Clk	: in	std_logic;
		FSL_Rst	: in	std_logic;
		FSL_S_Clk	: out	std_logic;
		FSL_S_Read	: out	std_logic;
		FSL_S_Data	: in	std_logic_vector(0 to 31);
		FSL_S_Control	: in	std_logic;
		FSL_S_Exists	: in	std_logic;
		-- DO NOT EDIT ABOVE THIS LINE ---------------------
		
		
		-- Added by DrK to implement VGA port pins 11/29/08
		hsync: out  std_logic;
		vsync: out  std_logic;
    rgb:   out std_logic_vector(0 to 2)
	
	);

attribute SIGIS : string; 
attribute SIGIS of FSL_Clk : signal is "Clk"; 
attribute SIGIS of FSL_S_Clk : signal is "Clk"; 
end snake;

------------------------------------------------------------------------------
-- Architecture Section
------------------------------------------------------------------------------

-- In this section, we povide an example implementation of ENTITY snake
-- that Read all inputs and add each input to the contents of register 'sum' which
-- acts as an accumulator
--
-- You will need to modify this example or implement a new architecture for
-- ENTITY snake to implement your coprocessor

architecture EXAMPLE of snake is

component graphics_top is
   port (
      clk,reset: in std_logic;
      hsync, vsync: out  std_logic;
		dot_x, dot_y: in std_logic_vector(4 downto 0);
		dot_val: in std_logic_vector(0 downto 0);
      rgb: out std_logic_vector(2 downto 0)
   );
end component;

   -- Total number of input data.
   constant NUMBER_OF_INPUT_WORDS  : natural := 1;

   type STATE_TYPE is (Idle, Read_Inputs);
   
   signal dot_x_number : std_logic_vector(4 downto 0);
   signal dot_y_number : std_logic_vector(4 downto 0);
   signal dot_val_number : std_logic_vector(0 downto 0);

   signal state        : STATE_TYPE;

   -- Counters to store the number inputs read
   signal nr_of_reads  : natural range 0 to NUMBER_OF_INPUT_WORDS - 1;

begin
   -- CAUTION:
   -- The sequence in which data are read in should be
   -- consistent with the sequence they are written in the
   -- driver's snake.c file

   FSL_S_Read  <= FSL_S_Exists   when state = Read_Inputs   else '0';
   The_SW_accelerator : process (FSL_Clk) is
   begin  -- process The_SW_accelerator
    if FSL_Clk'event and FSL_Clk = '1' then     -- Rising clock edge
      if FSL_Rst = '1' then               -- Synchronous reset (active high)
        -- CAUTION: make sure your reset polarity is consistent with the
        -- system reset polarity
        state        <= Idle;
		dot_x_number <= (others => '0');
		dot_y_number <= (others => '0');
		dot_val_number <= (others => '0');
        nr_of_reads  <= 0;
      else
        case state is
          when Idle =>
            if (FSL_S_Exists = '1') then
              state       <= Read_Inputs;
              nr_of_reads <= NUMBER_OF_INPUT_WORDS - 1;
              
            end if;

          when Read_Inputs =>
            if (FSL_S_Exists = '1') then
              -- Coprocessor function (Adding) happens here
              dot_x_number <= FSL_S_Data(21 to 25);
			  dot_y_number <= FSL_S_Data(26 to 30);
			  dot_val_number <= FSL_S_Data(31 to 31);
			  
              if (nr_of_reads = 0) then
                state        <= Idle;
              else
                nr_of_reads <= nr_of_reads - 1;
              end if;
            end if;

        end case;
      end if;
    end if;
   end process The_SW_accelerator;
   
   GAME_TOP_U1 : graphics_top
    port map(
      clk => FSL_Clk,
	  reset => FSL_Rst,
      hsync => hsync,
	  vsync => vsync,
	  dot_x => dot_x_number,
	  dot_y => dot_y_number,
	  dot_val => dot_val_number,
      rgb => rgb
   );
   
   
end architecture EXAMPLE;
