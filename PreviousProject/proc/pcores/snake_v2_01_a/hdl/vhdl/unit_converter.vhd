-- Listing 12.8
library ieee;
use ieee.std_logic_1164.ALL;
--use IEEE.STD_LOGIC_ARITH.ALL;
--use IEEE.STD_LOGIC_UNSIGNED.ALL;
use ieee.numeric_std.all;

entity unit_converter is
   port (
      pixel_x, pixel_y: in  std_logic_vector(9 downto 0);
      unit_x, unit_y: out  std_logic_vector(4 downto 0)
   );
end unit_converter;

architecture arch of unit_converter is
		signal unit_x_val : unsigned(9 downto 0);
		signal unit_y_val : unsigned(9 downto 0);
begin
		unit_x_val <= unsigned(pixel_x);
		unit_y_val <= unsigned(pixel_y);
		unit_x <= (pixel_x(9 downto 5)) when unit_x_val <= 639 
		else (others => '0');
		unit_y <= (pixel_y(9 downto 5)) when unit_y_val <= 479 
		else (others => '0');
end arch;