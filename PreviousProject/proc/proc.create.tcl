cd C:/xilinks_projs/final_proj2/proc
if { [xload new proc.xmp] != 0 } {
  exit -1
}
xset arch spartan3e
xset dev xc3s500e
xset package fg320
xset speedgrade -4
if { "unspecified" != "unspecified" } {
   xset hier unspecified
}
set xpsArch [xget arch]
if { ! [ string equal -nocase $xpsArch "spartan3e" ] } {
   puts "Device Family setting in XPS ($xpsArch) does not match Device Family setting in ISE (spartan3e)"
   exit -1
}
set xpsDev [xget dev]
if { ! [ string equal -nocase $xpsDev "xc3s500e" ] } {
   puts "Device setting in XPS ($xpsDev) does not match Device setting in ISE (xc3s500e)"
   exit -1
}
set xpsPkg [xget package]
if { ! [ string equal -nocase $xpsPkg "fg320" ] } {
   puts "Package setting in XPS ($xpsPkg) does not match Package setting in ISE (fg320)"
   exit -1
}
set xpsSpd [xget speedgrade]
if { ! [ string equal -nocase $xpsSpd "-4" ] } {
   puts "Speed Grade setting in XPS ($xpsSpd) does not match Speed Grade setting in ISE (-4)"
   exit -1
}
xset topinst proc_i
#default language
xset hdl vhdl
xset pnproj nplfile
xset intstyle ise
save proj
exit
