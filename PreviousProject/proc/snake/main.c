// Located in: microblaze_0/include/xparameters.h
#include "xparameters.h"

#include "stdio.h"
#include "stdlib.h"

#include "xbasic_types.h"
#include "xgpio.h"

//#include "gpio_header.h"



/* Added for VGA peripheral */
#include "snake.h"
#define input_slot_id   XPAR_FSL_SNAKE_0_INPUT_SLOT_ID

int Speed = 120000;
int total = 2;
XGpio buttons;

void sendData(unsigned int x, unsigned int y, unsigned int val)
{
	unsigned int datapacket[1];
	datapacket[0] = 0;
	datapacket[0] = (unsigned int) x << 6;
	datapacket[0] |= (unsigned int) y << 1;
	datapacket[0] |= (unsigned int) val;
	
	//Call the macro with instance specific slot IDs
	 snake(
		 input_slot_id,
		 datapacket    
		 );
}

struct unit{
       int x;
       int y;
       struct unit *next;
       };
       
typedef struct unit BodyPart;
       
typedef struct{
        BodyPart *head;
        BodyPart *tail;
        }Snake;

//Remove a tail from the snake
void removeTail(Snake *snk)
{
     BodyPart *curPart = snk->head;
     
     //Find second to last block
     while(curPart->next != snk->tail)
        curPart = curPart->next;
     
     //Unlink the tail
     curPart->next = NULL;

     
     //Free memory
     free(snk->tail);
     
     //Set old part to be the new tail
     snk->tail = curPart;
     
}

//Move the snake
//Returns:
// 0 -> Snake did not hit anything
// 1 -> Snake hit an apple
// -1 -> Snake hit a wall or itself
int moveSnake(Snake *snk, int dir, int FruitX, int FruitY, int keepFruit)
{
    //sendData(1, 0, 1);
	 BodyPart *newHead = (BodyPart *)malloc(sizeof(BodyPart));
    BodyPart *curPart = NULL;
	 
	 
    //sendData(snk->head->x, snk->head->y, 1);
	 //sendData(snk->tail->x, snk->tail->y, 1);
     //Make the new head
     switch(dir)
     {
                //Move North
                case 1:
                     newHead->x = snk->head->x;
                     newHead->y = snk->head->y - 1;
                     break;
                //Move East
                case 2:
                     newHead->x = snk->head->x+1;
                     newHead->y = snk->head->y;
                     break;
                //Move South
                case 3:
                     newHead->x = snk->head->x;
                     newHead->y = snk->head->y + 1;
                     break;
                //Move West
                case 4:
                     newHead->x = snk->head->x - 1;
                     newHead->y = snk->head->y;
                     break;
     }
     
     //Add it to the snake
     newHead->next = snk->head;
     snk->head = newHead;
     sendData(newHead->x, newHead->y, 1);


     //Check to see if the snake hit a fruit
     if(snk->head->x == FruitX && snk->head->y == FruitY)
        {
				total++;
				if (total%5 == 0)
					Speed -= Speed * .125;
				else
					Speed -= Speed * .0083;
				return 1;
			}

     //Check to see if the snake hit a wall
     if(snk->head->x == -1 || snk->head->x == 20
        || snk->head->y == -1 || snk->head->y == 15)
			return -1;
 
     //Check to see if the snake hit itself
     curPart = snk->head->next;
     while(curPart)
     {
         if(curPart->x == snk->head->x &&
               curPart->y == snk->head->y)
             return -1;
         
         curPart = curPart->next;
     }

     //Otherwise snake hit nothing
	  
	  //Check to see if we need to keep
	  //the tail bit active to display fruit
		if(!keepFruit)
		{	
			  //Update the screen
			  sendData(snk->tail->x, snk->tail->y, 0);
		}
		
     //Remove the tail
     removeTail(snk);
     
     return 0;
     
}                               

//Init the game
void initSnake(Snake *snake)
{
     //Create a head
     snake->head = (BodyPart *)(malloc(sizeof(BodyPart)));
     snake->head->x = 16;
     snake->head->y = 8;

	  
     //Create a tail
     snake->tail = (BodyPart *)(malloc(sizeof(BodyPart)));
     snake->tail->x = 15;
     snake->tail->y = 8;

	  
     //Set next pointers
     snake->tail->next = NULL;
     snake->head->next = snake->tail;
              
}

int dirUpdate(int dir)
{
	unsigned int buttonsIn;
	int newdir = dir;
	int lcount = 0 , rcount = 0, tcount = 0;
	static int upcount = 0;
	
   volatile int i;
	for(i=0;i<Speed;i++)
	{
		buttonsIn = XGpio_DiscreteRead(&buttons, 1);
		
		if(buttonsIn == 0 && upcount != 0)
		{
			upcount++;
			if(upcount > 1000)
			{
				upcount = 0;
			}
		}
		
		
	
		 if (buttonsIn == 8 && upcount == 0 )
		{
			lcount++;
			 
			 if(lcount > 5000)
			 {

				upcount = 1;
				
				  switch(dir)
				  {
						 //Move North -> West
						 case 1:
								newdir = 4;
								break;
						 //Move East -> North
						 case 2:
								newdir = 1;
								break;
						 //Move South -> West
						 case 3:
								newdir = 4;
								break;
						 //Move West -> South
						 case 4:
								newdir = 3;
								break;
				  }
					lcount = 0;
			  }
		}
		else if (buttonsIn == 4 && upcount == 0 )
		{
			rcount++;

			if(rcount > 5000)
			{

				upcount = 1;
					  switch(dir)
				  {
						 //Move North -> East
						 case 1:
								newdir = 2;
								break;
						 //Move East -> South
						 case 2:
								newdir = 3;
								break;
						 //Move South -> East
						 case 3:
								newdir = 2;
								break;
						 //Move West -> North
						 case 4:
								newdir = 1;
								break;
				  }
				  
				  rcount = 0;
			}
			
		}
		
		else if (buttonsIn == 1 && upcount == 0 )
		{
			tcount++;

			if(tcount > 5000)
			{
				Speed -= 10000;
				upcount = 1;
				tcount = 0;
			}
		}
	}
	return newdir;  
}

void blankScreen(void)
{
	int i, j;
	
	//Blank screen
	for (j=0;j<15;j++)
	{
		for (i=0;i<20;i++)
		{
			sendData(i,j,0);
		}
	}

	
}


void snakeclose(void)
{
	int i;
	
	blankScreen();
	
	for(i=0;i<7;i++)
	{
		sendData(i,5,1);
		sendData(i,11,1);
	}
	sendData(7,4,1);
	sendData(8,3,1);
	for(i=9;i<14;i++)
		sendData(i,2,1);
	sendData(14,3,1);
	sendData(15,4,1);
	sendData(16,5,1);
	sendData(17,6,1);
	for(i=7;i<11;i++)
		sendData(18,i,1);
	for(i=15;i<19;i++)
	{
		sendData(i,8,1);
		sendData(i,9,1);
	}
	sendData(15,13,1);
	for(i = 8;i<15;i++)
		sendData(i,14,1);
	sendData(6,12,1);
	sendData(7,13,1);
	sendData(11,4,1);
	sendData(12,4,1);
	sendData(17,11,1);
	sendData(16,12,1);
}

void snakeopen(void)
{
	int i;
	volatile j;	
	
	blankScreen();
	
	//open mouth!!!
	for(i=0;i<7;i++)
	{
		sendData(i,5,1);
		sendData(i,11,1);
	}
	sendData(7,4,1);
	sendData(8,3,1);
	for(i=9;i<14;i++)
		sendData(i,2,1);
	sendData(14,3,1);
	sendData(15,4,1);
	for(i=5;i<9;i++)
		sendData(16,i,1);
	for(i=14;i<17;i++)
	{
		sendData(i,8,1);
		sendData(i,10,1);
		sendData(i-4,6,1);
		sendData(i-4,12,1);
	}
	sendData(13,7,1);
	sendData(13,11,1);
	for(i = 6;i<13;i++)
		sendData(10,i,1);
	sendData(16,11,1);
	sendData(16,12,1);
	sendData(15,13,1);
	for(i=8;i<15;i++)
		sendData(i,14,1);
	sendData(7,13,1);
	sendData(6,12,1);
	sendData(11,4,1);
	sendData(12,4,1);


}

void freesnake(Snake *snake)
{
	  BodyPart * curPart = snake->head;
     BodyPart * tempPart = snake->head;
	  
	  while(curPart)
     {
         tempPart = curPart;
         curPart = curPart->next;
			free(tempPart);
     }
	  
	  snake->head = snake->tail = NULL;
	  
}

void drawScore(void)
{
	int i = 0;
	int j = 0;
	unsigned int buttonsIn;
	volatile int delay = 0;
	
	blankScreen();
	
	//Draw PTS
	for(i = 0; i<3; i++)
	{
		sendData(i, 0, 1);
		sendData(i, 2, 1);
		sendData(2, i, 1);
		sendData(i+3, 1, 1);
		sendData(i, 0, 1);
	}
	for(i = 0; i<4; i++)
	{
		sendData(0, i, 1);
		sendData(4, i, 1);
	}
		sendData(6, 0, 1);
		sendData(7, 0, 1);
		sendData(6, 1, 1);
		sendData(7, 2, 1);
		sendData(7, 3, 1);
		sendData(6, 3, 1);
	
	//Display score pyramid
	for(i = 0; i < 20 && total > 0; i ++)
	{
		for(j = 0; j <= i && total > 0; j++)
		{
			sendData(i,14-j,1);
			for(delay = 0;delay<300000;delay++);
			total--;
		}
	}
	
	//Wait for button press
	buttonsIn = 0;
	while(buttonsIn == 0)
		buttonsIn = XGpio_DiscreteRead(&buttons, 1);
	
}

void animSnake(void)
{
	unsigned int buttonsIn;
	int i = 0;
	
	buttonsIn = 0;
 
	while(buttonsIn == 0)
	{
		snakeclose();
		for(i = 0; i < 360000; i++)
			buttonsIn |= XGpio_DiscreteRead(&buttons, 1);
		snakeopen();
		for(i = 0; i < 360000; i++)
			buttonsIn |= XGpio_DiscreteRead(&buttons, 1);
	}
}

void lose(void)
{
	int i,j;
	volatile delay;
	unsigned int buttonsIn;

	buttonsIn = 0;
		
	
		//Blank screen
		for (j=0;j<15;j++)
		{
			for (i=0;i<20;i++)
			{
				sendData(i,j,1);
				for(delay = 0;delay<15000;delay++);
			}
		}
		
		drawScore();
		
	for (j=0;j<15;j++)
	{
		for (i=0;i<20;i++)
		{
			sendData(i,j,0);
			for(delay = 0;delay<10000;delay++);
		}
	}

}


int main(int argc, char *argv[])
{
	Xuint32 buttonsIn;
	int inStatus;
	
	//Keep track of direciton
	int direction = 1;
  
	//Keep track of if the game is running
	int running = 1;
  
	//Keep track of fruit
	int fruitX = 16;
	int fruitY = 3;
	int keepFruit = 0;


	
	inStatus = XGpio_Initialize(&buttons, XPAR_BUTTONS_4BIT_DEVICE_ID);

	if ((inStatus) != XST_SUCCESS)
		running = 0;
	else 
	{
		running = 1;
		XGpio_SetDataDirection(&buttons, 1, 0xF);
	}

  Snake *snake;
  	  snake  = (Snake *)(malloc(sizeof(Snake)));
	  
	while (1)
	{

		//Reset vars
		Speed = 120000;
		total = 2;
		
		fruitX = 16;
		fruitY = 3;
		
		direction = 1;
		
	  //Show animation
	  animSnake();
	  
	  //Blank the screen
	  blankScreen();
	  
	  //Display fruit
	  sendData(fruitX, fruitY, 1);
	  
	  //Initialize the snake
	  initSnake(snake);
	  sendData(snake->head->x,snake->head->y, 1);  
	  sendData(snake->tail->x, snake->tail->y, 1);
	  
	  
	  //Set Running
	  running = 1;
	  
	  //Pause before beginning
	  volatile int j = 0;
	  for(j =0; j < 2400000; j ++);
	  
	  //Main loop
	  while(running)
	  {
	  
		  int result = 0;
			
		  //Do delay loop here / update direction
		  direction = dirUpdate(direction);

		  //Move the snake
		  result = moveSnake(snake, direction, fruitX, fruitY, keepFruit);
	  
		 //Did the snake hit the fruit?
		  if(result == 1)
		  {
						//Set new fruit
						fruitX = snake->tail->x;
						fruitY = snake->tail->y;
						keepFruit = 1;
						
		  }
		  else if(result == -1)
		  {
				 //Did the snake hit the wall?
				 //End the game
				 lose();
				 freesnake(snake);
				 running = 0;
		  }
		  else
				keepFruit = 0;
						  
	  }
  }
  return 0;
}
