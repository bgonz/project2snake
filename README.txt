Snake
A CPEG 422 project by Billy Bednar and Brian Gonzalez.
Adapted from a previous project by Ryan Clemens and Joshua Suetterlein

Initial build
=============
1. Open Xilinx Platform Studio (EDK)
2. Go to File -> Open Project and open SnakeEDK/system.xmp
3. Click Project -> Export Hardware Design to SDK (or Export Design
   from the Navigator toolbar on the left) (this will automatically
   generate the netlist and bitstream). Leave the defaults and click
   Export & Launch SDK.
4. After 15 minutes or so the SDK will open
5. If prompted to select a workspace, select SnakeEDK/SDK/SDK_Export.
   Otherwise go to File -> Switch Workspace -> Other and select that
   location.
6. Click File -> Import and select Existing Projects into Workspace
   under general. Then click next.
7. Browse to SnakeEDK/SDK/SDK_Export. After closing the browse window a
   bunch of projects will appear preselected in the projects list.
   Click finish.
8. Open Xilinx Tools -> Configure JTAG Settings, change the type to
   Digilent USB Cable, and click OK.
9. Open Xilinx Tools -> Program FPGA. Select snake.elf and program.


Editing the vga IP core
=======================
1. From the Bus Interfaces tab of the System Assembly View in the EDK,
   right click the vga_fsl_0 IP and select Browse HDL Sources.
2. Select the file you want to edit.
3. Edit and save.
4. Project -> Rescan User Repositories
5. Hardware -> Clean Netlist
6. Project -> Export Hardware Design to SDK

