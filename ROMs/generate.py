#!/usr/bin/python
from PIL import Image
import sys

def processimage(filename, out):
   im = Image.open (filename)
   rgb_im = im.convert('RGB')
   pix = rgb_im.load()
   w, h = rgb_im.size
   print("%s: w= %d h= %d" % (filename, w, h))

   out.write("\t(\n")
   for y in range(h):
      out.write("\t\t(")
      for x in range(w):
         r = int(pix[x, y][0]/255.0 * 7 + 0.5)
         g = int(pix[x, y][1]/255.0 * 7 + 0.5)
         b = int(pix[x, y][2]/255.0 * 3 + 0.5)

         r = "{0:03b}".format(r)
         g = "{0:03b}".format(g)
         b = "{0:02b}".format(b)
         tmp = '%s%s%s' % (r,g,b)
         out.write(str(int(tmp, 2)))
         if x < w - 1:
            out.write(', ')
      if y < h - 1:
         out.write('),\n')
      else: 
         out.write(')\n')
   out.write('\t);\n')

def makerom(name):
   with open(name + ".rom.txt", 'w') as f:
      processimage(name + ".png", f)

makerom("snake_segment")
makerom("background")
makerom("fruit")
