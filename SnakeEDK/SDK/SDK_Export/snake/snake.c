#include <stdio.h>
#include "xparameters.h"
#include "xil_cache.h"
#include "xbasic_types.h"
#include "xgpio.h"
#include <stdlib.h>

/* adapted from previous snake project by ryan clemens and joshua suetterlein */

#include <stdlib.h>
#include "snake.h"
//#define vga_slot_id    XPAR_FSL_VGA_0_INPUT_SLOT_ID
#define vga_slot_id    0
#define GAME_DELAY     20000

#define UP 8
#define LEFT 4
#define DOWN 2
#define RIGHT 1

#define MIN_X 0
#define MIN_Y 0

#define NUM_ROWS 60
#define NUM_COLS 80/4
#define BORDER_WIDTH 1

#define MAX_X (NUM_COLS*4 - 1)
#define MAX_Y (NUM_ROWS - 1)

#define PIX_BACKGROUND 0b00
#define PIX_SNAKE 0b11
#define PIX_FRUIT 0b01
#define PIX_BORDER 0b10

#define GROW_AMOUNT 10


int Speed = GAME_DELAY*25;
int partsToAdd = 0;
Xuint16 total = 0;
volatile Xuint8 *video_bram = (Xuint8 *) XPAR_BRAM_0_BASEADDR;

struct unit{
	int x;
	int y;
	struct unit *next;
};

typedef struct unit BodyPart;

typedef struct {
	BodyPart *head;
	BodyPart *tail;
} Snake;

void drawPix(int x, int y, unsigned int texture) {
	switch(x % 4) {
	case 0:
		video_bram[NUM_COLS*(y) + (x)/4] = (video_bram[NUM_COLS*(y) + (x)/4] & (~(0b11 << 6))) | (texture << 6);
		break;
	case 1:
		video_bram[NUM_COLS*(y) + (x)/4] = (video_bram[NUM_COLS*(y) + (x)/4] & (~(0b11 << 4))) | (texture << 4);
		break;
	case 2:
		video_bram[NUM_COLS*(y) + (x)/4] = (video_bram[NUM_COLS*(y) + (x)/4] & (~(0b11 << 2))) | (texture << 2);
		break;
	case 3:
		video_bram[NUM_COLS*(y) + (x)/4] = (video_bram[NUM_COLS*(y) + (x)/4] & (~(0b11 << 0))) | (texture << 0);
		break;
	}
}

//Remove a tail from the snake
void removeTail(Snake *snk) {
	BodyPart *curPart = snk->head;

	//Find second to last block
	while(curPart->next != snk->tail) {
		curPart = curPart->next;
	}

	drawPix(snk->tail->x, snk->tail->y, PIX_BACKGROUND);
	//Unlink the tail
	curPart->next = NULL;
	//Free memory
	free(snk->tail);

	//Set old part to be the new tail
	snk->tail = curPart;

}

//Init the game
void initSnake(Snake *snake) {
	//Create a head
	snake->head = (BodyPart *)(malloc(sizeof(BodyPart)));
	snake->head->x = 3;
	snake->head->y = 4;


	//Create a tail
	snake->tail = (BodyPart *)(malloc(sizeof(BodyPart)));
	snake->tail->x = 2;
	snake->tail->y = 4;


	//Set next pointers
	snake->tail->next = NULL;
	snake->head->next = snake->tail;

}

// 0 -> Snake did not hit anything
// 1 -> Snake hit an apple
// -1 -> Snake hit a wall or itself
int moveSnake(Snake *snk, int dir, int fruitX, int fruitY) {

	BodyPart *newHead = (BodyPart *)malloc(sizeof(BodyPart));
	BodyPart *curPart = NULL;

	//Make the new head
	switch(dir) {

	case UP:
		newHead->x = snk->head->x;
		newHead->y = snk->head->y - 1;
		break;
	case RIGHT:
		newHead->x = snk->head->x+1;
		newHead->y = snk->head->y;
		break;
	case DOWN:
		newHead->x = snk->head->x;
		newHead->y = snk->head->y + 1;
		break;
	case LEFT:
		newHead->x = snk->head->x - 1;
		newHead->y = snk->head->y;
		break;
	}

	//Add it to the snake
	newHead->next = snk->head;
	snk->head = newHead;

	//Check to see if the snake hit a fruit
	if((snk->head->x == fruitX) && (snk->head->y == fruitY)) {
		total++;
		partsToAdd += GROW_AMOUNT - 1;
		//Speed -= Speed * !(total % 5) ? 0.2 : 0.05;
		Speed *= 0.97;
		return 1;
	}

	//Check to see if the snake hit a wall
	if((snk->head->x == MIN_X + BORDER_WIDTH - 1) || (snk->head->x == MAX_X - BORDER_WIDTH + 1)
			|| (snk->head->y == MIN_Y + BORDER_WIDTH - 1) || (snk->head->y == MAX_Y - BORDER_WIDTH + 1)){
		return -1;
	}

	//Check to see if the snake hit itself
	curPart = snk->head->next;
	while(curPart) {
		if(curPart->x == snk->head->x &&
				curPart->y == snk->head->y)
			return -1;

		curPart = curPart->next;
	}

	//Otherwise snake hit nothing

	//Remove the tail
	if (partsToAdd == 0) {
		removeTail(snk);
	} else {
		partsToAdd--;
	}

	return 0;

}

int dirUpdate(int dir, int dirIn) {

	int newdir = dir;

	if (dir == UP || dir == DOWN) {
		if (dirIn == LEFT || dirIn == RIGHT) {
			newdir = dirIn;
		}
	}
	else if (dir == LEFT || dir == RIGHT) {
		if (dirIn == UP || dirIn == DOWN) {
			newdir = dirIn;
		}
	}

	return newdir;
}

void freesnake(Snake *snake) {
	BodyPart * curPart = snake->head;
	BodyPart * tempPart = snake->head;

	while(curPart) {
		tempPart = curPart;
		if((curPart->x < MIN_X + BORDER_WIDTH) || (curPart->x > MAX_X - BORDER_WIDTH)
				|| (curPart->y < MIN_Y + BORDER_WIDTH) || (curPart->y > MAX_Y - BORDER_WIDTH)){
			drawPix(curPart->x, curPart->y, PIX_BORDER);
		}
		else {
			drawPix(curPart->x, curPart->y, PIX_BACKGROUND);
		}
		curPart = curPart->next;
		free(tempPart);
	}

	snake->head = snake->tail = NULL;

}

int main() {

	Xuint32 DataRead;
	Xuint32 Status;
	XGpio ButtonInput;
	volatile int Delay;
	int r, c;

	volatile Xuint16 *ssegment = (Xuint16 *) XPAR_SVN_SEG_AXI_0_BASEADDR;

	//Keep track of direction
	int direction = RIGHT;

	//Keep track of fruit
	int fruitX = rand() % (MAX_X - BORDER_WIDTH) + BORDER_WIDTH;
	int fruitY = rand() % (MAX_Y - BORDER_WIDTH) + BORDER_WIDTH;

	// Initialize buttons
	Status = XGpio_Initialize(&ButtonInput, XPAR_PUSH_BUTTONS_4BITS_DEVICE_ID);
	if (Status != XST_SUCCESS) print("PANIC! Gpio Initialize FAILED.\r\n");

	// Set the direction to be all inputs
	XGpio_SetDataDirection(&ButtonInput, 1, 0xFFFFFFFF);

	Snake *snake;
	snake  = (Snake *)(malloc(sizeof(Snake)));

	initSnake(snake);

	// draw background
	for (r = 0; r < NUM_ROWS; r++) {
		for (c = 0; c < NUM_COLS*4; c++) {
			drawPix(c, r, PIX_BORDER);
		}
	}
	for (r = BORDER_WIDTH; r < NUM_ROWS - BORDER_WIDTH; r++) {
		for (c = BORDER_WIDTH; c < NUM_COLS*4 - BORDER_WIDTH; c++) {
			drawPix(c, r, PIX_BACKGROUND);
		}
	}

	while(1) {
		//Call the FSL peripheral to display game objects
		//vga_fsl(vga_slot_id, score);

		// user input
		DataRead = XGpio_DiscreteRead(&ButtonInput, 1);

		int result = 0;

		direction = dirUpdate(direction, DataRead);
		result = moveSnake(snake, direction, fruitX, fruitY);

		//Did the snake hit the fruit?
		if (result == 1) {
			//Set new fruit
			fruitX = rand() % (MAX_X - BORDER_WIDTH) + BORDER_WIDTH;
			fruitY = rand() % (MAX_Y - BORDER_WIDTH) + BORDER_WIDTH;

		}
		else if(result == -1)
		{
			//Did the snake hit the wall?
			//End the game
			freesnake(snake);

			total = 0;
			partsToAdd = 0;
			direction = RIGHT;
			Speed = GAME_DELAY*25;
			initSnake(snake);
		}

		// draw snake
		BodyPart * curPart = snake->head;
		while(curPart) {
			drawPix(curPart->x, curPart->y, PIX_SNAKE);
			curPart = curPart->next;
		}

		// draw fruit
		drawPix(fruitX, fruitY, PIX_FRUIT);

		// display score
		ssegment[0] = (((total / 1000) % 10) << 12) | (((total / 100) % 10) << 8) | (((total / 10) % 10) << 4) | (((total % 10)) << 0);


		// Wait a small amount of time or the ball will move too fast!
		for (Delay = 0; Delay < Speed; Delay++);

	}

	return 0;
}
