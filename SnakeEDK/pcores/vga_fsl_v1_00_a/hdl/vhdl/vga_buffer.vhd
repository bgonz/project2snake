-- Display buffer for VGA screen

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.snake_roms.all;

entity vga_buffer is
	port(
		video_on         : in std_logic;
		pixel_x, pixel_y : in std_logic_vector(9 downto 0);
		graph_rgb        : out std_logic_vector(7 downto 0);
		-- BRAM Ports
		bram_enb         : out std_logic;
		bram_addrb       : out std_logic_vector(31 downto 0);
		bram_doutb       : in  std_logic_vector(31 downto 0);
		bram_wenb        : out std_logic_vector(3 downto 0);
		bram_dinb        : out std_logic_vector(31 downto 0);
		bram_rstb        : out std_logic
	);
end vga_buffer;

architecture arch of vga_buffer is
	signal bram_data_buffer : std_logic_vector(31 downto 0);
	signal bram_addr_buffer : std_logic_vector(31 downto 0);

	signal block_x : integer;
	signal block_y : integer;
	signal block_index : std_logic_vector(12 downto 0);
	signal index_within_read : std_logic_vector(3 downto 0);
	signal block_value : std_logic_vector(1 downto 0);

	signal background_x : integer;
	signal background_y : integer;
	signal background_pixel : std_logic_vector(7 downto 0);
	signal subblock_x : integer;
	signal subblock_y : integer;
	signal snake_pixel : std_logic_vector(7 downto 0);
	signal fruit_pixel : std_logic_vector(7 downto 0);
	signal foreground_pixel : std_logic_vector(7 downto 0);
begin
	bram_wenb <= (others => '0');  -- Always disable writing, only c can write to the bram
	bram_dinb <= (others => '0');
	bram_rstb <= '0';              -- Always disable reset

	-- Background & object generation
	background_x <= to_integer(unsigned(pixel_x)) mod BACKGROUND_WIDTH;
	background_y <= to_integer(unsigned(pixel_y)) mod BACKGROUND_HEIGHT;
	background_pixel <= std_logic_vector(to_unsigned(BACKGROUND_ROM(background_y, background_x), 8));
	snake_pixel <= std_logic_vector(to_unsigned(SNAKE_BODY_ROM(subblock_y, subblock_x), 8));
	fruit_pixel <= std_logic_vector(to_unsigned(FRUIT_ROM(subblock_y, subblock_x), 8));

	-- Output
	graph_rgb <=
		x"00" when video_on = '0' else
		background_pixel when foreground_pixel = "00000000" else
		foreground_pixel;

	-- Divide the 640x480 screen into 80x60 grid of 8x8 blocks
	block_x <= to_integer(unsigned(pixel_x(9 downto 3)));
	block_y <= to_integer(unsigned(pixel_y(8 downto 3)));
	subblock_x <= to_integer(unsigned(pixel_x(2 downto 0)));
	subblock_y <= to_integer(unsigned(pixel_y(2 downto 0)));
	block_index <= std_logic_vector(to_unsigned(80*block_y + block_x, 13));

	-- Enable bram when in the active area
	bram_enb <= '1' when video_on = '1' else '0';

	-- The bram data bus is 32 bits wide so we get 16 blocks at a time
	bram_addr_buffer <= x"41410" & "0" & block_index(12 downto 2);
	bram_addrb <= bram_addr_buffer and x"FFFFFFFC"; -- mask out the sub 16-block group address
	bram_data_buffer <= bram_doutb when video_on = '1' else (others => '0');
	index_within_read <= block_index(3 downto 0);

	process(video_on, bram_data_buffer)
	begin
		case index_within_read is
			when "0000" => block_value <= bram_data_buffer( 7 downto  6);
			when "0001" => block_value <= bram_data_buffer( 5 downto  4);
			when "0010" => block_value <= bram_data_buffer( 3 downto  2);
			when "0011" => block_value <= bram_data_buffer( 1 downto  0);
			when "0100" => block_value <= bram_data_buffer(15 downto 14);
			when "0101" => block_value <= bram_data_buffer(13 downto 12);
			when "0110" => block_value <= bram_data_buffer(11 downto 10);
			when "0111" => block_value <= bram_data_buffer( 9 downto  8);
			when "1000" => block_value <= bram_data_buffer(23 downto 22);
			when "1001" => block_value <= bram_data_buffer(21 downto 20);
			when "1010" => block_value <= bram_data_buffer(19 downto 18);
			when "1011" => block_value <= bram_data_buffer(17 downto 16);
			when "1100" => block_value <= bram_data_buffer(31 downto 30);
			when "1101" => block_value <= bram_data_buffer(29 downto 28);
			when "1110" => block_value <= bram_data_buffer(27 downto 26);
			when "1111" => block_value <= bram_data_buffer(25 downto 24);
			when others => report "unreachable" severity failure;
		end case;

		case block_value is
			when "00" => foreground_pixel <= background_pixel;
			when "01" => foreground_pixel <= fruit_pixel;
			when "10" => foreground_pixel <= "11100000";
			when "11" => foreground_pixel <= snake_pixel;
			when others => report "unreachable" severity failure;
		end case;
	end process;
end arch;
