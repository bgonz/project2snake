------------------------------------------------------------------------------
-- vga_fsl - entity/architecture pair
------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

-------------------------------------------------------------------------------------
--
--
-- Definition of Ports
-- FSL_Clk             : Synchronous clock
-- FSL_Rst           : System reset, should always come from FSL bus
-- FSL_S_Clk       : Slave asynchronous clock
-- FSL_S_Read      : Read signal, requiring next available input to be read
-- FSL_S_Data      : Input data
-- FSL_S_CONTROL   : Control Bit, indicating the input data are control word
-- FSL_S_Exists    : Data Exist Bit, indicating data exist in the input FSL bus
--
-------------------------------------------------------------------------------

------------------------------------------------------------------------------
-- Entity Section
------------------------------------------------------------------------------

entity vga_fsl is
	port 
	(	-- VGA Ports
		vga_clk : in  std_logic;
		hsync	: out std_logic;
		vsync	: out std_logic;
		rgb		: out std_logic_vector(7 downto 0);
		-- BRAM Ports
    --BRAM_CLK_B 	: out std_logic;
        BRAM_RST_B 	: out std_logic;
		BRAM_EN_B 	: out std_logic;
		BRAM_WEN_B 	: out std_logic_vector(0 to 3);
		BRAM_Addr_B : out std_logic_vector(0 to 31);
		BRAM_Din_B 	: in std_logic_vector(0 to 31);
		BRAM_Dout_B : out std_logic_vector(0 to 31);
		-- DO NOT EDIT BELOW THIS LINE ---------------------
		-- Bus protocol ports, do not add or delete. 
		FSL_Clk		  : in	std_logic;
		FSL_Rst		  : in	std_logic;
		FSL_S_Clk	  : in	std_logic;
		FSL_S_Read	  : out	std_logic;
		FSL_S_Data	  : in	std_logic_vector(0 to 31);
		FSL_S_Control : in	std_logic;
		FSL_S_Exists  : in	std_logic
		-- DO NOT EDIT ABOVE THIS LINE ---------------------
	);

  attribute SIGIS : string; 
  attribute SIGIS of FSL_Clk : signal is "Clk"; 
  attribute SIGIS of FSL_S_Clk : signal is "Clk"; 
  --attribute SIGIS of BRAM_CLK_B : signal is "Clk";
  attribute SIGIS of vga_clk : signal is "Clk";
end vga_fsl;

------------------------------------------------------------------------------
-- Architecture Section
------------------------------------------------------------------------------

-- In this section, we povide an example implementation of ENTITY vga_fsl
-- that Read all inputs and add each input to the contents of register 'sum' which
-- acts as an accumulator
--
-- You will need to modify this example or implement a new architecture for
-- ENTITY vga_fsl to implement your coprocessor

architecture EXAMPLE of vga_fsl is

	component vga_top is
    generic (OBJECT_SIZE : natural := 10);
    port (
      clk, reset: in std_logic;
      hsync, vsync: out  std_logic;
      rgb: out std_logic_vector(7 downto 0);
      score : in std_logic_vector(0 to 15);

      -- BRAM Ports
      bram_enb    : out std_logic;
      bram_addrb  : out std_logic_vector(0 to 31);
      bram_doutb  : in  std_logic_vector(0 to 31);
      bram_wenb   : out std_logic_vector(0 to 3);
      bram_dinb   : out std_logic_vector(0 to 31);
      bram_rstb   : out std_logic
	);
	end component;  

   -- This game has two objects and position will not exceed 1023
   -- because VGA screen resolution is 640x480.
   constant OBJECT_SIZE : natural := 10; -- max. coordinate value is 1024

   signal score_signal, score_todisplay : std_logic_vector(0 to 15);
   -- This is used to signal loading the objects for display
   signal objects_done : std_logic;
   
   -- Total number of input data.
   constant NUMBER_OF_INPUT_WORDS  : natural := 1;

   type STATE_TYPE is (Idle, Read_Inputs);

   signal state        : STATE_TYPE;

   -- Counters to store the number inputs read
   signal nr_of_reads  : natural range 0 to NUMBER_OF_INPUT_WORDS - 1;

begin
   -- CAUTION:
   -- The sequence in which data are read in should be
   -- consistent with the sequence they are written in the
   -- driver's vga_fsl.c file

   FSL_S_Read  <= FSL_S_Exists when state = Read_Inputs   else '0';
   The_SW_accelerator : process (FSL_Clk) is
   begin  -- process The_SW_accelerator
    if rising_edge(FSL_Clk) then     -- Rising clock edge
      if FSL_Rst = '1' then               -- Synchronous reset (active high)
        -- CAUTION: make sure your reset polarity is consistent with the
        -- system reset polarity
        state        <= Idle;
        score_signal <= (others => '0');
        objects_done <= '0';
        nr_of_reads  <= 0;
      else
        case state is
          when Idle =>
            if (FSL_S_Exists = '1') then
              state       <= Read_Inputs;
              nr_of_reads <= NUMBER_OF_INPUT_WORDS - 1;
			     score_signal <= (others => '0');
              objects_done <= '0';
            end if;

          when Read_Inputs =>
            if (FSL_S_Exists = '1') then
              -- Coprocessor function (Adding) happens here
              score_signal <= FSL_S_Data(16 to 31);
              if (nr_of_reads = 0) then
                objects_done <= '1';
                state        <= Idle;
              else
                objects_done <= '0';  
                nr_of_reads <= nr_of_reads - 1;
              end if;
            end if;

        end case;
      end if;
    end if;
   end process The_SW_accelerator;
   
      -- Process to copy object positions to buffer that gets displayed...
   COPY_OBJECTS: process (FSL_Clk) is
   begin
    if rising_edge(FSL_Clk) then     -- Rising clock edge
      if FSL_Rst = '1' then               -- Synchronous reset (active high)
        -- CAUTION: make sure your reset polarity is consistent with the
        -- system reset polarity
        score_todisplay <= (others => '0');
      elsif objects_done = '1' then
        score_todisplay <= score_signal;
      end if;
    end if;
   end process;

   -- Instance the VGA component
   GAME_TOP_U1: vga_top port map (
    clk => vga_clk,
    reset => FSL_Rst,
    hsync => hsync,
    vsync => vsync,
    rgb => rgb,
    score => score_todisplay,
    -- BRAM signals
    bram_enb => BRAM_EN_B, 
    bram_addrb => BRAM_Addr_B,
    bram_doutb => BRAM_Din_B,
    bram_wenb => BRAM_WEN_B,
    bram_dinb => BRAM_Dout_B,
    bram_rstb => BRAM_RST_B
    );
   
  --BRAM_CLK_B <= FSL_Clk;
  
end architecture EXAMPLE;
